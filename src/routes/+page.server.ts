import type { PageServerLoad } from './$types';
import { sql } from '@vercel/postgres';

process.env.POSTGRES_URL = process.env.POSTGRES_URL || import.meta.env.VITE_POSTGRES_URL;
process.env.POSTGRES_URL_NON_POOLING = process.env.POSTGRES_URL_NON_POOLING || import.meta.env.VITE_POSTGRES_URL_NON_POOLING;
process.env.POSTGRES_PRISMA_URL = process.env.POSTGRES_PRISMA_URL || import.meta.env.VITE_POSTGRES_PRISMA_URL;
process.env.POSTGRES_USER = process.env.POSTGRES_USER || import.meta.env.VITE_POSTGRES_USER;
process.env.POSTGRES_HOST = process.env.POSTGRES_HOST || import.meta.env.VITE_POSTGRES_HOST;
process.env.POSTGRES_PASSWORD = process.env.POSTGRES_PASSWORD || import.meta.env.VITE_POSTGRES_PASSWORD;
process.env.POSTGRES_DATABASE = process.env.POSTGRES_DATABASE || import.meta.env.VITE_POSTGRES_DATABASE;



sql`SELECT * from SONGS`
  .then(v=> console.log(v));



export const load = (async () => {
  return {
    songs: (await sql`SELECT * from SONGS`).rows,
  };
}) satisfies PageServerLoad;
